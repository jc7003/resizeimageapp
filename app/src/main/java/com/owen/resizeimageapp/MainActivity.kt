package com.owen.resizeimageapp

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var bitmap: Bitmap? = null

    private var imagePath: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_CANCELED) return

        // Result code is RESULT_OK only if the user selects an Image
        when (requestCode) {
            GALLERY_REQUEST_CODE -> {
                imagePath = data?.data ?: return

                contentResolver.openInputStream(imagePath!!)?.use { imageStream ->
                    imageview.setImageBitmap(BitmapFactory.decodeStream(imageStream).also { bitmap = it })

                    showInfo()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_select.setOnClickListener { pickFromGallery() }

        btn_resize.setOnClickListener { resizeImage()  }
    }

    private fun pickFromGallery() {
        //Create an Intent with action as ACTION_PICK
        val intent = Intent(Intent.ACTION_PICK)
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.type = "image/*"
        // Launching the Intent
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    private fun resizeImage() {
        //case 1: use path.
        val options = BitmapFactory.Options()
        // get origin image info
        contentResolver.openInputStream(imagePath!!)?.use { imageStream ->
            // get info only size
            options.inJustDecodeBounds = true
            BitmapFactory.decodeStream(imageStream, null, options)

            Log.d("MainActivity", "witdh: ${options.outWidth}" )

            // set resize rate
            options.inSampleSize = 2
            options.inJustDecodeBounds = false
        }

        contentResolver.openInputStream(imagePath!!)?.use { imageStream ->
            bitmap = BitmapFactory.decodeStream(imageStream, null, options)
        }


        //case 2: use bitmap
//        bitmap = Bitmap.createScaledBitmap(bitmap!!, bitmap!!.width/2, bitmap!!.height/2, true)
        imageview.setImageBitmap(bitmap)

        showInfo()
    }

    private fun showInfo() {
        textview_info.text = "image size: witdh:${bitmap?.width}, height: ${bitmap?.height}, size:${bitmap?.byteCount}"
    }

    companion object {
        const val GALLERY_REQUEST_CODE = 1001
    }
}
